Source: rocsparse
Section: devel
Homepage: https://github.com/ROCm/rocSPARSE
Priority: optional
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rocm-team/rocsparse.git
Vcs-Browser: https://salsa.debian.org/rocm-team/rocsparse
Maintainer: Debian ROCm Team <debian-ai@lists.debian.org>
Uploaders: Maxime Chambonnet <maxzor@maxzor.eu>,
           Cordell Bloor <cgmb@slerp.xyz>,
           Étienne Mollier <emollier@debian.org>,
           Christian Kastner <ckk@debian.org>,
           Xuanteng Huang <xuanteng.huang@outlook.com>,
	   Kari Pahula <kaol@debian.org>,
Build-Depends: debhelper-compat (= 13),
               cmake,
               gfortran,
               hipcc (>= 5.6.1~),
               libamd-comgr-dev (>= 6.0~),
               libhsa-runtime-dev (>= 5.7.1~),
               rocm-cmake (>= 5.3.0),
               librocprim-dev (>= 5.5.1~),
               python3,
               python3-yaml,
               libgtest-dev <!nocheck>,
Build-Depends-Indep: dh-sequence-sphinxdoc <!nodoc>,
               doxygen <!nodoc>,
               python3-breathe <!nodoc>,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               python3-sphinx-design <!nodoc>,
               python3-sphinx-copybutton <!nodoc>,
               libjs-jquery <!nodoc>,
               libjs-mathjax <!nodoc>,
               libjs-sphinxdoc <!nodoc>,
               libjs-underscore <!nodoc>,
               python3-myst-nb <!nodoc>,
Rules-Requires-Root: no

Package: librocsparse0
Section: libs
Architecture: amd64 arm64 ppc64el
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: ROCm library for sparse linear algebra - library
 rocSPARSE is a library that implements BLAS operations for sparse data
 structures. It is built on the AMD ROCm platform and optimized for
 discrete AMD GPUs.
 .
 rocSPARSE provides a C99 API containing sparse level 1, sparse level 2 and
 sparse level 3 BLAS functions, as well as general matrix multiplication
 between dense and sparse matricies (GEMMI), sparse preconditioner functions,
 sparse matrix format conversion functions, and sparse matrix reordering
 functions.
 .
 It supports a variety of sparse matrix formats, including coordinate list
 in either array of structure form (COO) or structure of array form (COO SOA),
 compressed sparse row (CSR) or column (CSC), block compressed sparse row
 (BSR), general block compressed sparse row (GEBSR), and the ELLPACK-ITPACK
 format (ELL).
 .
 This package provides the AMD ROCm rocSPARSE library.

Package: librocsparse-dev
Section: libdevel
Architecture: amd64 arm64 ppc64el
Depends: librocsparse0 (= ${binary:Version}),${misc:Depends}, ${shlibs:Depends},
Recommends: libamdhip64-dev
Suggests: librocsparse-doc
Description: ROCm library for sparse linear algebra - headers
 rocSPARSE is a library that implements BLAS operations for sparse data
 structures. It is built on the AMD ROCm platform and optimized for
 discrete AMD GPUs.
 .
 rocSPARSE provides a C99 API containing sparse level 1, sparse level 2 and
 sparse level 3 BLAS functions, as well as general matrix multiplication
 between dense and sparse matricies (GEMMI), sparse preconditioner functions,
 sparse matrix format conversion functions, and sparse matrix reordering
 functions.
 .
 It supports a variety of sparse matrix formats, including coordinate list
 in either array of structure form (COO) or structure of array form (COO SOA),
 compressed sparse row (CSR) or column (CSC), block compressed sparse row
 (BSR), general block compressed sparse row (GEBSR), and the ELLPACK-ITPACK
 format (ELL).
 .
 This package provides the AMD ROCm rocSPARSE development headers.

Package: librocsparse0-tests
Section: libdevel
Architecture: amd64 arm64 ppc64el
Build-Profiles: <!nocheck>
Depends: librocsparse0 (= ${binary:Version}),${misc:Depends}, ${shlibs:Depends},
         librocsparse0-tests-data
Description: ROCm library for sparse linear algebra - tests
 rocSPARSE is a library that implements BLAS operations for sparse data
 structures. It is built on the AMD ROCm platform and optimized for
 discrete AMD GPUs.
 .
 rocSPARSE provides a C99 API containing sparse level 1, sparse level 2 and
 sparse level 3 BLAS functions, as well as general matrix multiplication
 between dense and sparse matricies (GEMMI), sparse preconditioner functions,
 sparse matrix format conversion functions, and sparse matrix reordering
 functions.
 .
 It supports a variety of sparse matrix formats, including coordinate list
 in either array of structure form (COO) or structure of array form (COO SOA),
 compressed sparse row (CSR) or column (CSC), block compressed sparse row
 (BSR), general block compressed sparse row (GEBSR), and the ELLPACK-ITPACK
 format (ELL).
 .
 This package provides the AMD ROCm rocSPARSE test binaries used for verifying
 that the library is functioning correctly. These binaries are only used to
 check whether rocSPARSE works correctly on a given GPU, so generally speaking,
 users probably don't need this package.

Package: librocsparse0-tests-data
Section: libdevel
Architecture: all
Build-Profiles: <!nocheck>
Depends: ${misc:Depends},
Description: ROCm library for sparse linear algebra - test data
 rocSPARSE is a library that implements BLAS operations for sparse data
 structures. It is built on the AMD ROCm platform and optimized for
 discrete AMD GPUs.
 .
 rocSPARSE provides a C99 API containing sparse level 1, sparse level 2 and
 sparse level 3 BLAS functions, as well as general matrix multiplication
 between dense and sparse matricies (GEMMI), sparse preconditioner functions,
 sparse matrix format conversion functions, and sparse matrix reordering
 functions.
 .
 It supports a variety of sparse matrix formats, including coordinate list
 in either array of structure form (COO) or structure of array form (COO SOA),
 compressed sparse row (CSR) or column (CSC), block compressed sparse row
 (BSR), general block compressed sparse row (GEBSR), and the ELLPACK-ITPACK
 format (ELL).
 .
 This package provides the data files required by the AMD ROCm rocSPARSE tests.
 The tests are only used to check whether rocSPARSE works correctly on a given
 GPU, so generally speaking, users probably don't need this package.

Package: librocsparse-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax,
Replaces: librocsparse-dev (<< 5.7.1-3~)
Breaks: librocsparse-dev (<< 5.7.1-3~)
Description: ROCm library for sparse linear algebra - documentation
 rocSPARSE is a library that implements BLAS operations for sparse data
 structures. It is built on the AMD ROCm platform and optimized for
 discrete AMD GPUs.
 .
 rocSPARSE provides a C99 API containing sparse level 1, sparse level 2 and
 sparse level 3 BLAS functions, as well as general matrix multiplication
 between dense and sparse matricies (GEMMI), sparse preconditioner functions,
 sparse matrix format conversion functions, and sparse matrix reordering
 functions.
 .
 It supports a variety of sparse matrix formats, including coordinate list
 in either array of structure form (COO) or structure of array form (COO SOA),
 compressed sparse row (CSR) or column (CSC), block compressed sparse row
 (BSR), general block compressed sparse row (GEBSR), and the ELLPACK-ITPACK
 format (ELL).
 .
 This package provides the AMD ROCm rocSPARSE documentation.
